<?php

include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
   protected $db = null;  
    
    /**
   * @throws PDOException
     */
    public function __construct($db= null)   
  {  
       if ($db) 
        {
            $this->db = $db;
        }
        else
        {
            try {                                                                           //If it works: 
            $this->db = new PDO('mysql:host=127.0.0.1;dbname=book_base', 'root', '');        //connection to the database via PDO-object
            //string (what driver we will use, the host and name of database), username(root since running on local computer), password(blank)
            
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);              //Setting a specific error mode
            
            } catch(PDOException $e) {                                                      //If it doesn't work: Catch the error
                echo $e->getMessage();                                                      //Uses PDO to output the error message (getMessage() function)
                die();                                                                      //Kill the page
            }

         }
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
   * @throws PDOException
     */
    public function getBookList()
    {
       $booklist = array();

        try{
            foreach($this->db->query("SELECT title, author, description, ID FROM books") as $book){
            $booklist[] = new Book($book['title'], $book['author'], $book['description'], $book['ID']); 
            }
        }catch(PDOException $e){
            echo $e->getMessage(); 
        }
        

        return $booklist;

   }
    
    
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
   * @throws PDOException
     */
      public function getBookById($id) {
      $book = null;
      if(filter_var($id, FILTER_VALIDATE_INT)){
        
      try {
        $pstmt = $this->db->prepare('SELECT * FROM books WHERE ID=:ID');
        $pstmt->bindParam(':ID', $id, PDO::PARAM_INT);
        $pstmt->execute();
        $book = $pstmt->fetch(PDO::FETCH_ASSOC);
      } catch(PDOException $e) {
          echo $e->getMessage();
      }

      if($book == FALSE)  {
        return null;
      }

      return $book = new Book( $book['title'], $book['author'], $book['description'], $book['ID']);
    } else {
      $view = new ErrorView('This ID does not exist'); 
      $view->create();
    }
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
   * @throws PDOException
     */
    public function addBook($book)
    {

        $title = $book->title; 
        $author = $book->author;
        $description = $book->description;
        try{
        if($book->author != '' && $book->title != ''){
          if ($book->description == '') {$book->description = null;}
            $sql = "INSERT INTO books (title, author, description) VALUES (:title, :author, :description)";    //We need to bind data to the parametres :title, :author, :description, :id. This WILL protect the database from SQL injection. //({$name}, {$author}, {$description})";
            $query = $this->db->prepare($sql);                                        //query($sql)
          //The difference between using prepare and query, is that prepare does not execute anything

        } else {
          $view = new ErrorView('The fields Title and Author can not be empty');
          $view->create(); 
        }
        
        
            $query->execute(array( //We are passing an array that we want to bind to the query and executing the statement
            ':title' => $title,
            ':author' => $author,
            ':description' => $description));
        }catch(PDOException $e){
            echo $e->getMessage(); 
        }   

       

    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        $title = $book->title; 
        $author = $book->author;
        $description = $book->description;
        try{

        $query = $this->db->prepare("UPDATE books SET title = :title, author = :author, description = :description WHERE id = :id"); 
        if($book->author!= '' && $book->title != ''){
          if($book->description == ''){ 
            $book->description = null;
          }
          $query->execute(array(':title'=>$title, ':author'=>$author, ':description'=>$description,':id'=>$book->id));
        } else{
          $view = new ErrorView('The fields Title and Author can not be empty');
          $view->create(); 
        }        

        } catch(PDOException $e){
            echo $e->getMessage(); 
        }


    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
      try{
        $sql = "DELETE FROM books WHERE (id = :id)"; 
        $query = $this->db->prepare($sql); 
        $query->execute(array(':id' => $id));
      } catch(PDOException $e) {
        echo $e->getMessage();
      }

        
    }
  
}

?>